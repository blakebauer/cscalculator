# CSCalcutor

This is the repository for the CSCalculator by Blake Bauer. It is a calculator app designed for Computer Scienctists and Software Engineers. It was made for the senior individual project at Western Oregon University for a Bachelor's degree in Computer Science/Mathematics.

#### Vision Statement 0.1

---

> For programmers who need a calculator designed for them, Calcutron is a calculator that is designed for use by Computer Scientists and Software Engineers. Unlike current calculator apps, Calcutron will provide the ability to do arithmetic in a variety of bases such as hexadecimal and binary. It will allow for bitwise and logic operations, matrix calculations, and a table output of a function. It could give the ability to do list or array calculations, and a way to write scripts or loops and list the variables at each step.

---

##### Blake Bauer
+ [Portfolio](https://bbauer15.github.io/CS460/)
+ [Team Project](https://bitbucket.org/blakebauer/sneakysoftware/overview) 