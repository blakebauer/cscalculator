package edu.wou.blake.calcutron;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Binary.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Binary#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Binary extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private String binInput;

    String[] binFunctions = {
            "min", "max", "mod", "abs", "lcm", "gcd"
    };

    public Binary() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Binary.
     */
    // TODO: Rename and change types and number of parameters
    public static Binary newInstance(String param1, String param2) {
        Binary fragment = new Binary();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_binary, container, false);

        binInput = "";

        ArrayAdapter adapter = new ArrayAdapter<String>(this.getContext(), R.layout.activity_functions, binFunctions);
        ListView lv = ((ListView) root.findViewById(R.id.binFunctions));
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setBinInput(binInput + binFunctions[position] + "(");
                ((ConstraintLayout) getView().findViewById(R.id.binMainLayer)).setClickable(true);
                ConstraintLayout fl = ((ConstraintLayout) getView().findViewById(R.id.binFunctionlayer));
                fl.setVisibility(View.INVISIBLE);
                fl.setClickable(false);
            }
        });

        BinClickListener h = new BinClickListener();
        ((Button) root.findViewById(R.id.binBackspace)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.binClear)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.binComma)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.binEquals)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.binFunctionBtn)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.binMod)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.binNumber0)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.binNumber1)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.binNOT)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.binOR)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.binXOR)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.binAND)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.binPlus)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.binMinus)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.binMultiply)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.binParenL)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.binParenR)).setOnClickListener(h);

        return root;
    }

    private class BinClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.binBackspace:
                    binBackspace(v);
                    break;
                case R.id.binClear:
                    binClear(v);
                    break;
                case R.id.binEquals:
                    binEvaluate(v);
                    break;
                case R.id.binFunctionBtn:
                   binFunction(v);
                   break;

                case R.id.binNOT:
                case R.id.binOR:
                case R.id.binXOR:
                case R.id.binAND:
                    binBitwise(v);
                    break;
                case R.id.binMod:
                case R.id.binNumber1:
                case R.id.binNumber0:
                case R.id.binPlus:
                case R.id.binMinus:
                case R.id.binMultiply:
                case R.id.binParenL:
                case R.id.binParenR:
                case R.id.binComma:
                    binOperator(v);
                    break;
            }
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onBinaryFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onBinaryFragmentInteraction(Uri uri);
    }

    public void binOperator(View view){
        String operator = ((TextView)view).getText().toString();
        setBinInput(binInput + operator);
    }

    public void binEvaluate(View view){
        try {
            int result = binProcess(binInput);
            ((TextView) getView().findViewById(R.id.binResult)).setText(Integer.toBinaryString(result));
        } catch (Exception e) {
            ((TextView) getView().findViewById(R.id.binResult)).setText("Error with input");
        }
    }

    public void binClear(View view){
        setBinInput("");
        ((TextView) getView().findViewById(R.id.binResult)).setText("");
    }

    public void binBackspace(View view){
        if (binInput.length() > 0)
            setBinInput(binInput.substring(0, binInput.length() - 1));
    }

    public void binFunction(View view){
        ((ConstraintLayout) getView().findViewById(R.id.binMainLayer)).setClickable(false);
        ConstraintLayout fl = ((ConstraintLayout) getView().findViewById(R.id.binFunctionlayer));
        fl.setVisibility(View.VISIBLE);
        fl.setClickable(true);
    }

    private void setBinInput(String str){
        binInput = str;
        ((TextView) getView().findViewById(R.id.binInput)).setText(binInput);
    }

    private int binProcess(String str) throws Exception{
        BinaryInterpreter i = new BinaryInterpreter(str);
        int x = i.Evaluate();
        return x;
    }

    public void binBitwise(View view){
        String operator = ((TextView)view).getText().toString();
        switch(operator){
            case "OR |":
                setBinInput(binInput + '|');
                break;
            case "AND &":
                setBinInput(binInput + '&');
                break;
            case "XOR ^":
                setBinInput(binInput + '^');
                break;
            case "NOT ~":
                setBinInput(binInput + '~');
                break;
        }
    }
}
