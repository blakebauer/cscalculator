package edu.wou.blake.calcutron;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.Dictionary;
import java.util.Hashtable;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Decimal.OnDecFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Decimal#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Decimal extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnDecFragmentInteractionListener mListener;

    String[] functions = {
            "sqrt", "log", "min", "max", "round", "floor", "ceil", "mod", "abs", "lcm", "gcd", "sin",
            "cos", "tan", "asin", "acos", "atan", "rad", "deg"
    };

    Dictionary<String, String> fns = new Hashtable<>();
    Dictionary<String, Double> vars = new Hashtable<>();

    private String input;

    public Decimal() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Decimal.
     */
    // TODO: Rename and change types and number of parameters
    public static Decimal newInstance(String param1, String param2) {
        Decimal fragment = new Decimal();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View root = inflater.inflate(R.layout.fragment_decimal, container, false);

        ArrayAdapter adapter = new ArrayAdapter<String>(this.getContext(), R.layout.activity_functions, functions);
        ListView lv = ((ListView) root.findViewById(R.id.functions));
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setInput(input + functions[position] + "(");
                ((ConstraintLayout) getView().findViewById(R.id.mainlayer)).setClickable(true);
                ConstraintLayout fl = ((ConstraintLayout) getView().findViewById(R.id.functionlayer));
                fl.setVisibility(View.INVISIBLE);
                fl.setClickable(false);
            }
        });

        DecClickListener h = new DecClickListener();

        ((Button) root.findViewById(R.id.vars)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.fn)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.backspace)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.clear)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.comma)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.equals)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.functionBtn)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.mod)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.number1)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.number2)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.number3)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.number4)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.number5)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.number6)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.number7)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.number8)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.number9)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.number0)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.plus)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.minus)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.multiply)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.parenL)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.parenR)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.divid)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.dot)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.newFn)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.newVar)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.fnDone)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.varDone)).setOnClickListener(h);

        return root;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onDecFragmentInteraction(uri);
        }
    }

    private class DecClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.vars:
                    vars(v);
                    break;
                case R.id.fn:
                    fns(v);
                    break;
                case R.id.backspace:
                    backspace(v);
                    break;
                case R.id.clear:
                    clear(v);
                    break;
                case R.id.equals:
                    evaluate(v);
                    break;
                case R.id.functionBtn:
                   function(v);
                   break;
                case R.id.mod:
                case R.id.comma:
                case R.id.number1:
                case R.id.number2:
                case R.id.number3:
                case R.id.number4:
                case R.id.number5:
                case R.id.number6:
                case R.id.number7:
                case R.id.number8:
                case R.id.number9:
                case R.id.number0:
                case R.id.plus:
                case R.id.minus:
                case R.id.multiply:
                case R.id.parenL:
                case R.id.parenR:
                case R.id.divid:
                case R.id.dot:
                    operatorClick(v);
                    break;

                case R.id.newFn:
                    addFns(v);
                    break;
                case R.id.newVar:
                    addVar(v);
                    break;
                case R.id.fnDone:
                    genFns(v);
                    break;
                case R.id.varDone:
                    genVars(v);
                    break;
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnDecFragmentInteractionListener) {
            mListener = (OnDecFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnDecFragmentInteractionListener {
        // TODO: Update argument type and name
        void onDecFragmentInteraction(Uri uri);
    }

    public void genFns(View view){
        fns = new Hashtable<>();
        TableLayout t = (TableLayout) (view.getParent().getParent());
        for(int i = 1; i < t.getChildCount()-1; i++){
            TableRow r = (TableRow) t.getChildAt(i);
            String name = ((EditText)r.getChildAt(1)).getText().toString();
            String body = ((EditText)r.getChildAt(3)).getText().toString();
            fns.put(name, body);
        }

        ((ConstraintLayout) getView().findViewById(R.id.mainlayer)).setClickable(true);
        ConstraintLayout fl = ((ConstraintLayout) getView().findViewById(R.id.fnslayer));
        fl.setVisibility(View.INVISIBLE);
        fl.setClickable(false);
    }

    public void fns(View view){
        ((ConstraintLayout) getView().findViewById(R.id.mainlayer)).setClickable(false);
        ConstraintLayout fl = ((ConstraintLayout) getView().findViewById(R.id.fnslayer));
        fl.setVisibility(View.VISIBLE);
        fl.setClickable(true);
    }

    public void addFns(View view){
        TableLayout fl = ((TableLayout) getView().findViewById(R.id.fnsTable));
        TableRow newF = new TableRow(this.getContext());

        EditText name = new EditText(this.getContext());
        name.setText("fn");

        TextView tag = new TextView(this.getContext());
        tag.setText("(x)=");

        EditText body = new EditText(this.getContext());
        body.setText("x");

        Button btn = new Button(this.getContext());
        btn.setText("-");
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewGroup v = (ViewGroup) (((View) view.getParent()).getParent());
                v.removeView((View) view.getParent());
            }
        });

        Button btn2 = new Button(this.getContext());
        btn2.setText("*");
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                genFns(view);
                setInput(input + ((EditText)((TableRow)view.getParent()).getChildAt(1)).getText().toString() + "(");
            }
        });

        newF.addView(btn2);
        newF.addView(name);
        newF.addView(tag);
        newF.addView(body);
        newF.addView(btn);

        btn.getLayoutParams().width = 5;
        btn2.getLayoutParams().width = 5;

        fl.addView(newF, fl.getChildCount()-1);
    }

    public void genVars(View view){
        vars = new Hashtable<>();
        TableLayout t = (TableLayout) (view.getParent().getParent());
        for(int i = 1; i < t.getChildCount()-1; i++){
            TableRow r = (TableRow) t.getChildAt(i);
            String name = ((EditText)r.getChildAt(1)).getText().toString();
            String body = ((EditText)r.getChildAt(3)).getText().toString();
            vars.put(name, Double.valueOf(body));
        }

        ((ConstraintLayout) getView().findViewById(R.id.mainlayer)).setClickable(true);
        ConstraintLayout fl = ((ConstraintLayout) getView().findViewById(R.id.varslayer));
        fl.setVisibility(View.INVISIBLE);
        fl.setClickable(false);
    }

    public void vars(View view){
        ((ConstraintLayout) getView().findViewById(R.id.mainlayer)).setClickable(false);
        ConstraintLayout fl = ((ConstraintLayout) getView().findViewById(R.id.varslayer));
        fl.setVisibility(View.VISIBLE);
        fl.setClickable(true);
    }

    public void addVar(View view){
        TableLayout fl = ((TableLayout) getView().findViewById(R.id.varsTable));
        TableRow newF = new TableRow(this.getContext());

        EditText name = new EditText(this.getContext());
        name.setText("pi");

        TextView tag = new TextView(this.getContext());
        tag.setText("=");

        EditText body = new EditText(this.getContext());
        body.setText("3.14");

        Button btn = new Button(this.getContext());
        btn.setText("-");
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewGroup v = (ViewGroup) (((View) view.getParent()).getParent());
                v.removeView((View) view.getParent());
            }
        });

        Button btn2 = new Button(this.getContext());
        btn2.setText("*");
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                genVars(view);
                setInput(input + ((EditText)((TableRow)view.getParent()).getChildAt(1)).getText().toString());
            }
        });

        newF.addView(btn2);
        newF.addView(name);
        newF.addView(tag);
        newF.addView(body);
        newF.addView(btn);

        btn.getLayoutParams().width = 5;
        btn2.getLayoutParams().width = 5;

        fl.addView(newF, fl.getChildCount()-1);
    }

    public void operatorClick(View view){
        String operator = ((TextView)view).getText().toString();
        setInput(input + operator);
    }

    public void evaluate(View view){
        try {
            double result = process(input);
            ((TextView) getView().findViewById(R.id.result)).setText("" + result);
        } catch (Exception e) {
            e.printStackTrace();
            ((TextView) getView().findViewById(R.id.result)).setText("Error with input");
        }
    }

    public void clear(View view){
        setInput("");
        ((TextView) getView().findViewById(R.id.result)).setText("");
    }

    public void backspace(View view){
        if (input.length() > 0)
            setInput(input.substring(0, input.length() - 1));
    }

    public void function(View view){
        ((ConstraintLayout) getView().findViewById(R.id.mainlayer)).setClickable(false);
        ConstraintLayout fl = ((ConstraintLayout) getView().findViewById(R.id.functionlayer));
        fl.setVisibility(View.VISIBLE);
        fl.setClickable(true);
    }

    private void setInput(String str){
        input = str;
        ((TextView) getView().findViewById(R.id.input)).setText(input);
    }

    private double process(String str) throws Exception{
        Interpreter i = new Interpreter(str, fns, vars);
        double x = i.Evaluate();
        return x;
    }
}
