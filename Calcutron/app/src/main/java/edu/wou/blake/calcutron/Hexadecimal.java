package edu.wou.blake.calcutron;


import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Hexadecimal#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Hexadecimal extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private OnHexFragmentInteractionListener mListener;

    private String hexInput;

    public Hexadecimal() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Hexadecimal.
     */
    // TODO: Rename and change types and number of parameters
    public static Hexadecimal newInstance(String param1, String param2) {
        Hexadecimal fragment = new Hexadecimal();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_hexadecimal, container, false);
        hexInput = "";
        HexClickListener h = new HexClickListener();
        ((Button) root.findViewById(R.id.hexBackspace)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexClear)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexComma)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexEquals)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexFunctionBtn)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexMod)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexNumber)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexNumber1)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexNumber2)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexNumber3)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexNumber4)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexNumber5)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexNumber6)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexNumber7)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexNumber8)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexNumber9)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexNumber10)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexNumber11)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexNumber12)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexNumber13)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexNumber14)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexNumber0)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexNOT)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexOR)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexXOR)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexAND)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexPlus)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexMinus)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexMultiply)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexParenL)).setOnClickListener(h);
        ((Button) root.findViewById(R.id.hexParenR)).setOnClickListener(h);

        return root;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnHexFragmentInteractionListener) {
            mListener = (OnHexFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnHexFragmentInteractionListener {
        // TODO: Update argument type and name
        void onHexFragmentInteraction(Uri uri);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onHexFragmentInteraction(uri);
        }
    }

    private class HexClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.hexBackspace:
                    hexBackspace(v);
                    break;
                case R.id.hexClear:
                    hexClear(v);
                    break;
                case R.id.hexEquals:
                    hexEvaluate(v);
                    break;
                //case R.id.hexFunctionBtn:
                 //   hexFunction(v);
                 //   break;

                case R.id.hexNOT:
                case R.id.hexOR:
                case R.id.hexXOR:
                case R.id.hexAND:
                    hexBitwise(v);
                    break;
                case R.id.hexMod:
                case R.id.hexNumber:
                case R.id.hexNumber1:
                case R.id.hexNumber2:
                case R.id.hexNumber3:
                case R.id.hexNumber4:
                case R.id.hexNumber5:
                case R.id.hexNumber6:
                case R.id.hexNumber7:
                case R.id.hexNumber8:
                case R.id.hexNumber9:
                case R.id.hexNumber10:
                case R.id.hexNumber11:
                case R.id.hexNumber12:
                case R.id.hexNumber13:
                case R.id.hexNumber14:
                case R.id.hexNumber0:
                case R.id.hexPlus:
                case R.id.hexMinus:
                case R.id.hexMultiply:
                case R.id.hexParenL:
                case R.id.hexParenR:
                case R.id.hexComma:
                    hexOperator(v);
                    break;
            }
        }
    }

    //Hexadecimal
    public void hexOperator(View view) {
        String operator = ((TextView) view).getText().toString();
        setHexInput(hexInput + operator);
    }

    public void hexEvaluate(View view) {
        try {
            int result = hexProcess(hexInput);
            ((TextView) getView().findViewById(R.id.hexResult)).setText(Integer.toHexString(result));
        } catch (Exception e) {
            ((TextView) getView().findViewById(R.id.hexResult)).setText(e.getMessage());
        }
    }

    public void hexClear(View view) {
        setHexInput("");
        ((TextView) getView().findViewById(R.id.hexResult)).setText("");
    }

    public void hexBackspace(View view) {
        if (hexInput.length() > 0)
            setHexInput(hexInput.substring(0, hexInput.length() - 1));
    }

    public void hexFunction(View view) {
        ((ConstraintLayout) getView().findViewById(R.id.hexMainLayer)).setClickable(false);
        ConstraintLayout fl = ((ConstraintLayout) getView().findViewById(R.id.hexFunctionlayer));
        fl.setVisibility(View.VISIBLE);
        fl.setClickable(true);
    }

    private void setHexInput(String str) {
        hexInput = str;
        ((TextView) getView().findViewById(R.id.hexInput)).setText(hexInput);
    }

    private int hexProcess(String str) throws Exception {
        HexInterpreter i = new HexInterpreter(str);
        int x = i.Evaluate();
        return x;
    }

    public void hexBitwise(View view) {
        String operator = ((TextView) view).getText().toString();
        switch (operator) {
            case "OR |":
                setHexInput(hexInput + '|');
                break;
            case "AND &":
                setHexInput(hexInput + '&');
                break;
            case "XOR ^":
                setHexInput(hexInput + '^');
                break;
            case "NOT ~":
                setHexInput(hexInput + '~');
                break;
        }
    }
}
