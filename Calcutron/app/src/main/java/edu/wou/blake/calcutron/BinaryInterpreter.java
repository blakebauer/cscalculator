package edu.wou.blake.calcutron;

import java.util.Stack;

/**
 * Created by blake on 3/13/2018.
 */

public class BinaryInterpreter {

    private String expression;
    private int pos = -1;
    private char curr;

    public BinaryInterpreter(String str){
        expression = str;
    }

    public int Evaluate(){
        incPos();
        int x = Bitwise();
        if (pos < expression.length())
            throw new RuntimeException("Unexpected symbol: " + curr);
        return x;
    }

    private void incPos(){
        pos++;
        if(pos < expression.length())
            curr = expression.charAt(pos);
        else
            curr = (char) -1;
    }

    private boolean consume(char c){
        if(curr == c){
            incPos();
            return true;
        }
        return false;
    }

    private int Bitwise(){
        if(consume('~'))
            return ~Bitwise();

        int x = Expression();
        while(true){
            if(consume('&'))
                x = x & Expression();
            else if(consume('|'))
                x = x | Expression();
            else if(consume('^'))
                x = x ^ Expression();
            else
                return x;
        }
    }

    private int Expression(){
        int x = Term();
        while(true){
            if(consume('+'))
                x += Term();
            else if(consume('-'))
                x -= Term();
            else
                return x;
        }
    }

    private int Term(){
        int x = Factor();
        while(true){
            if(consume('*'))
                x *= Factor();
            else if(consume('%'))
                x %= Factor();
            else
                return x;
        }
    }

    private int Factor(){
        if(consume('-')) return -Factor();

        int x;
        int startPos = this.pos;
        if(consume('(')){
            x = Bitwise();
            consume(')');
        } else if (curr == '0' || curr == '1'){
            while(curr == '0' || curr == '1')
                incPos();
            x = Integer.parseInt(expression.substring(startPos, this.pos), 2);
        } else if (curr >= 'a' && curr <= 'z'){
            while(curr >= 'a' && curr <= 'z')
                incPos();
            String func = expression.substring(startPos, this.pos);
            Stack<Integer> params = new Stack<>();
            do {
                params.push(Factor());
            } while (consume(','));
            consume(')');
            x = Function(func, params);
        }else {
            throw new RuntimeException("Unexpected symbol: " + curr);
        }

        return x;
    }

    private int parseBinary(String substring) {
        int x = 0;
        int s = 0;

        for(int i = substring.length()-1; i >= 0; i--){
            x += substring.charAt(i) == '1' ? Math.pow(2, s) : 0;
            s++;
        }

        return x;
    }

    private int Function(String func, Stack<Integer> params){
        int o = 0;
        int f = params.pop();
        if (func.equals("min")) o = Math.min(f, params.pop());
        else if (func.equals("max")) o = Math.max(f, params.pop());
        else if (func.equals("mod")) o = f % params.pop();
        else if (func.equals("abs")) o = Math.abs(f);
        else if (func.equals("lcm")) o = lcm(f, params.pop());
        else if (func.equals("gcd")) o = gcd(f, params.pop());
        else throw new RuntimeException("Unknown function: " + func);
        return o;
    }

    private int gcd(int a, int b){
        while (b > 0)
        {
            int temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }

    private int lcm(int a, int b) {
        return a * (b / gcd(a, b));
    }

}
