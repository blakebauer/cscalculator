package edu.wou.blake.calcutron;

import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Stack;

/**
 * Created by blake on 3/10/2018.
 */

public class Interpreter {
    private String expression;
    Dictionary<String, String> fns;
    Dictionary<String, Double> vars;
    private int pos = -1;
    private char curr;

    public Interpreter(String str, Dictionary<String, String> fns, Dictionary<String, Double> vars) {
        this.fns = fns;
        this.vars = vars;
        expression = str;
    }

    public double Evaluate() {
        incPos();
        double x = Expression();
        if (pos < expression.length())
            throw new RuntimeException("Unexpected symbol: " + curr);
        return x;
    }

    private void incPos() {
        pos++;
        if (pos < expression.length())
            curr = expression.charAt(pos);
        else
            curr = (char) -1;
    }

    private boolean consume(char c) {
        if (curr == c) {
            incPos();
            return true;
        }
        return false;
    }

    private double Expression() {
        double x = Term();
        while (true) {
            if (consume('+'))
                x += Term();
            else if (consume('-'))
                x -= Term();
            else
                return x;
        }
    }

    private double Term() {
        double x = Factor();
        while (true) {
            if (consume('*'))
                x *= Factor();
            else if (consume('/'))
                x /= Factor();
            else if (consume('%'))
                x %= Factor();
            else
                return x;
        }
    }

    private double Factor() {
        if (consume('-')) return -Factor();

        double x;
        int startPos = this.pos;
        if (consume('(')) {
            x = Expression();
            consume(')');
        } else if ((curr >= '0' && curr <= '9') || curr == '.') {
            while ((curr >= '0' && curr <= '9') || curr == '.')
                incPos();
            x = Double.parseDouble(expression.substring(startPos, this.pos));
        } else if (curr >= 'a' && curr <= 'z') {
            while (curr >= 'a' && curr <= 'z')
                incPos();
            String func = expression.substring(startPos, this.pos);
            if (consume('(')) {
                Stack<Double> params = new Stack<>();
                do {
                    params.push(Factor());
                } while (consume(','));
                consume(')');
                x = Function(func, params);
            } else {
                x = vars.get(func);
            }
        } else {
            throw new RuntimeException("Unexpected symbol: " + curr);
        }

        if (consume('^'))
            x = Math.pow(x, Factor());

        return x;
    }

    private double Function(String func, Stack<Double> params) {
        double o = 0.0;
        double f = params.pop();
        if (func.equals("sqrt")) o = Math.sqrt(f);
        else if (func.equals("log") && !params.isEmpty()) o = Math.log(f) / Math.log(params.pop());
        else if (func.equals("log")) o = Math.log(f);
        else if (func.equals("min")) o = Math.min(f, params.pop());
        else if (func.equals("max")) o = Math.max(f, params.pop());
        else if (func.equals("round")) o = Math.round(f);
        else if (func.equals("floor")) o = Math.floor(f);
        else if (func.equals("ceil")) o = Math.ceil(f);
        else if (func.equals("mod")) o = f % params.pop();
        else if (func.equals("abs")) o = Math.abs(f);
        else if (func.equals("lcm")) o = lcm(f, params.pop());
        else if (func.equals("gcd")) o = gcd(f, params.pop());
        else if (func.equals("sin")) o = Math.sin(f);
        else if (func.equals("cos")) o = Math.cos(f);
        else if (func.equals("tan")) o = Math.tan(f);
        else if (func.equals("asin")) o = Math.asin(f);
        else if (func.equals("acos")) o = Math.acos(f);
        else if (func.equals("atan")) o = Math.atan(f);
        else if (func.equals("rad")) o = Math.toRadians(f);
        else if (func.equals("deg")) o = Math.toDegrees(f);
        else if (fns.get(func) != null) {
            Interpreter i = new Interpreter(fns.get(func).replaceAll("x", "" + f), new Hashtable<String, String>(), vars);
            o = i.Evaluate();
        } else throw new RuntimeException("Unknown function: " + func);
        return o;
    }

    private double gcd(double a, double b) {
        while (b > 0) {
            double temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }

    private double lcm(double a, double b) {
        return a * (b / gcd(a, b));
    }


}
