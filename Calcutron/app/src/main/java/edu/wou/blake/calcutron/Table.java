package edu.wou.blake.calcutron;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Space;
import android.widget.TabHost;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Table.OnTableFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Table#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Table extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnTableFragmentInteractionListener mListener;
    Dictionary<String, String> fns = new Hashtable<>();

    public Table() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Table.
     */
    // TODO: Rename and change types and number of parameters
    public static Table newInstance(String param1, String param2) {
        Table fragment = new Table();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_table, container, false);

        TabHost host = (TabHost) root.findViewById(R.id.tableTabHost);
        host.setup();
        host.setOnTabChangedListener(new OnTabChangeListener());

        TabHost.TabSpec spec = host.newTabSpec("Input");
        spec.setContent(R.id.tableInputTab);
        spec.setIndicator("Input");
        host.addTab(spec);

        spec = host.newTabSpec("Table");
        spec.setContent(R.id.tableOutputTab);
        spec.setIndicator("Table");
        host.addTab(spec);
        host.setCurrentTab(0);

        Button b = root.findViewById(R.id.newTableFn);
        b.setOnClickListener(new OnBtnClick());

        return root;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onTableFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnTableFragmentInteractionListener) {
            mListener = (OnTableFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnTableFragmentInteractionListener {
        // TODO: Update argument type and name
        void onTableFragmentInteraction(Uri uri);
    }

    private class OnBtnClick implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            addFns(v);
        }
    }

    private class OnTabChangeListener implements TabHost.OnTabChangeListener {

        @Override
        public void onTabChanged(String tabId) {
            System.out.println("#########" + tabId);
            if (tabId.equals("Table")) {
                fns = new Hashtable<>();
                TableLayout t = (TableLayout) (getView().findViewById(R.id.inputTable));
                t.setStretchAllColumns(true);
                for (int i = 1; i < t.getChildCount() - 1; i++) {
                    TableRow r = (TableRow) t.getChildAt(i);
                    String name = ((EditText) r.getChildAt(1)).getText().toString();
                    String body = ((EditText) r.getChildAt(3)).getText().toString();
                    fns.put(name, body);
                }

                TableLayout o = (TableLayout) (getView().findViewById(R.id.tableOuput));
                o.removeAllViews();
                TableRow row = new TableRow(getContext());

                row.addView(newTableTextView("input", false));

                Enumeration<String> e = fns.keys();

                while (e.hasMoreElements()) {
                    String name = e.nextElement();
                    row.addView(newTableTextView(name, false));
                }

                o.addView(row);
                int start = Integer.valueOf(((EditText) getView().findViewById(R.id.inputStart)).getText().toString());
                int end = Integer.valueOf(((EditText) getView().findViewById(R.id.inputEnd)).getText().toString());
                int increment = Integer.valueOf(((EditText) getView().findViewById(R.id.inputStep)).getText().toString());

                for (int i = start; i <= end; i += increment) {
                    row = new TableRow(getContext());
                    row.addView(newTableTextView("" + i, true));

                    e = fns.keys();

                    while (e.hasMoreElements()) {
                        String name = e.nextElement();
                        String body = fns.get(name);

                        Interpreter in = new Interpreter(name + "(" + i + ")", fns, new Hashtable<String, Double>());
                        double value = in.Evaluate();
                        row.addView(newTableTextView("" + value, true));
                    }

                    o.addView(row);
                }


            }
        }
    }

    private TextView newTableTextView2(String text, boolean end) {
        TextView t = new TextView(getContext());
        t.setPaddingRelative(20, 0, 20, 0);
        t.setTextAppearance(getContext(), R.style.TextAppearance_AppCompat_Medium);
        t.setTextColor(Color.BLACK);
        if (end) t.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
        t.setText(text);
        return t;
    }

    private TextView newTableTextView(String text, boolean end) {
        BorderedTextView t = new BorderedTextView(getContext());
        t.setPaddingRelative(20, 0, 20, 0);
        t.setTextAppearance(getContext(), R.style.TextAppearance_AppCompat_Medium);
        t.setTextColor(Color.BLACK);
        if (end) t.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
        t.setText(text);
        return t;
    }

    public void genFns(View view) {

    }

    public void addFns(View view) {
        TableLayout fl = ((TableLayout) getView().findViewById(R.id.inputTable));
        TableRow newF = new TableRow(this.getContext());

        EditText name = new EditText(this.getContext());
        name.setText("fn");

        TextView tag = new TextView(this.getContext());
        tag.setText("(x)=");

        EditText body = new EditText(this.getContext());
        body.setText("x");

        Button btn = new Button(this.getContext());
        btn.setText("-");
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewGroup v = (ViewGroup) (((View) view.getParent()).getParent());
                v.removeView((View) view.getParent());
            }
        });

        Space s = new Space(this.getContext());

        newF.addView(s);
        newF.addView(name);
        newF.addView(tag);
        newF.addView(body);
        newF.addView(btn);

        btn.getLayoutParams().width = 5;

        fl.addView(newF, fl.getChildCount() - 1);
    }

    public class BorderedTextView extends android.support.v7.widget.AppCompatTextView {
        private Paint paint = new Paint();

        public BorderedTextView(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
            init();
        }

        public BorderedTextView(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        public BorderedTextView(Context context) {
            super(context);
            init();
        }

        private void init() {
            paint.setStyle(Paint.Style.STROKE);
            paint.setColor(Color.BLACK);
            paint.setStrokeWidth(4);
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawLine(0, 0, getWidth(), 0, paint);

            canvas.drawLine(getWidth(), 0, getWidth(), getHeight(), paint);

            canvas.drawLine(0, getHeight(), getWidth(), getHeight(), paint);

            canvas.drawLine(0, 0, 0, getHeight(), paint);
        }
    }
}
